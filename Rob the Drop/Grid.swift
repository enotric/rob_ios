//
//  InvisibleGrid.swift
//  Rob the Drop
//
//  Created by Kenneth Meier on 29.10.16.
//  Copyright © 2016 Enotric Studios UG (haftungsbeschränkt). All rights reserved.
//

import Foundation
import GameplayKit
import SpriteKit

class Grid {
    
    let dimensions = 10
    
    var node_grid : GKGridGraph<GKGridGraphNode>
    
    init(){
        //Initialize the Graph, with no walls
        node_grid = GKGridGraph(fromGridStartingAt: int2(0, 0), width: Int32(dimensions), height: Int32(dimensions), diagonalsAllowed: true)
    }
    
}
